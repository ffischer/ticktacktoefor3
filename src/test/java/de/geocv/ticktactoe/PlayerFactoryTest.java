package de.geocv.ticktactoe;

import com.google.inject.Guice;
import com.google.inject.Injector;
import de.geocv.ticktactoe.Board.BoardTicTacToe;
import de.geocv.ticktactoe.Board.SquareBoard;
import de.geocv.ticktactoe.modules.GamePropertiesModule;
import de.geocv.ticktactoe.player.BasePlayer;
import de.geocv.ticktactoe.player.PlayerFactory;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

class PlayerFactoryTest {
    Injector injector = Guice.createInjector(new GamePropertiesModule());
    SquareBoard board = injector.getInstance(BoardTicTacToe.class);
    final String file = "./playersTest.json";
    final String jsonData ="   [\n" +
            "    {\n" +
            "      \"name\"       : \"johanne\",\n" +
            "      \"sign\"       : \"j\",\n" +
            "      \"playerType\" : \"HUMAN\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\"       : \"fabian\",\n" +
            "      \"sign\"       : \"f\",\n" +
            "      \"playerType\" : \"HUMAN\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\"       : \"PC\",\n" +
            "      \"sign\"       : \"k\",\n" +
            "      \"playerType\" : \"KI\",\n" +
            "      \"kiMode\"     : \"RANDOM\"\n" +
            "    }\n" +
            "  ]";
    @BeforeEach
    void setUp() {
        try {
            FileUtils.writeStringToFile(new File(file), jsonData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    void tearDown(){
        try {
            File jsonFile = new File(file);
            jsonFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Test
    void readPlayerConfig() {
        try {
            ArrayList<BasePlayer> players = PlayerFactory.create(file,board);
            Assertions.assertTrue(players.size() == 3);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
