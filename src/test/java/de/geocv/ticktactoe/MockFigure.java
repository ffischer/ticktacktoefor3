package de.geocv.ticktactoe;

import de.geocv.ticktactoe.Board.Figure;

public class MockFigure implements Figure {

    @Override
    public char getChar() {
        return 'K';
    }
}
