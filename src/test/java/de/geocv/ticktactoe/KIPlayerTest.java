package de.geocv.ticktactoe;

import com.google.inject.Guice;
import com.google.inject.Injector;
import de.geocv.ticktactoe.Board.BoardTicTacToe;
import de.geocv.ticktactoe.Board.SquareBoard;
import de.geocv.ticktactoe.modules.GamePropertiesModule;
import de.geocv.ticktactoe.player.BasePlayer;
import de.geocv.ticktactoe.player.KIPlayer;
import de.geocv.ticktactoe.player.PlayerFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class KIPlayerTest {
    Injector injector = Guice.createInjector(new GamePropertiesModule());
    SquareBoard board;
    BasePlayer ki;
    @BeforeEach
    void setUp() {
        board =  injector.getInstance(BoardTicTacToe.class);
        ki = new KIPlayer("ki",'X', PlayerFactory.KIMode.RANDOM);
        ki.setBoard(board);
    }

    @AfterEach
    void tearDown() {
        board = null;
        ki = null;
    }

    @Test
    void moveStep() {
        boolean hasFound = false;
        ki.moveStep(ki);
        for(int x = 0; x < board.getSize(); x++){
            for(int y = 0; y < board.getSize(); y++){
                if( board.getFigure(x,y) != null && board.getFigure(x,y).getChar() == 'X')
                    hasFound = true;
            }
        }
        Assertions.assertTrue(hasFound,"KI has not set a board-field");
    }
}
