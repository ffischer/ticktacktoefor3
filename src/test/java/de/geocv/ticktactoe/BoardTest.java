package de.geocv.ticktactoe;

import com.google.inject.Guice;
import com.google.inject.Injector;
import de.geocv.ticktactoe.Board.BoardTicTacToe;
import de.geocv.ticktactoe.Board.Figure;
import de.geocv.ticktactoe.Board.SquareBoard;
import de.geocv.ticktactoe.modules.GamePropertiesModule;
import org.junit.jupiter.api.*;

class MockFigureBoard implements Figure {
    char f;
    public MockFigureBoard(char figureSign) {
        f = figureSign;
    }

    public char getChar() {
        return f;
    }
}

@DisplayName("Board-Tests")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BoardTest {

    private Injector injector = Guice.createInjector(new GamePropertiesModule());


    private SquareBoard gameBoard;

    private MockFigureBoard changedFigure;
    @BeforeAll
    void initGameBoard(){
        gameBoard = injector.getInstance(BoardTicTacToe.class);
        changedFigure  = new MockFigureBoard('C');

        fillValues(gameBoard);
    }
    @Test
    void testReturnOfSetFigureByOverwritingValue(){
        Assertions.assertFalse(gameBoard.setFigure(changedFigure,0,0),"Board.setFigure returns the wrong bool-val");
        changedFigure = (MockFigureBoard) gameBoard.getFigure(0,0);
        Assertions.assertTrue(gameBoard.getFigure(0,0).getChar() == 'X',"Board.setFigure wrote the new sign instead of it's read-only");

    }

    @Test
    void testOutOfRange() {
        Assertions.assertFalse(gameBoard.setFigure(changedFigure,12,12),"OutOfRange doesn't work!");
    }

    void fillValues(SquareBoard b){
        MockFigureBoard defaultFigure = new MockFigureBoard('X');

        for(int x = 0; x < gameBoard.getSize(); x++){
            for(int y = 0; y < gameBoard.getSize(); y++){
               gameBoard.setFigure(defaultFigure,x,y);
            }
        }
    }
}
