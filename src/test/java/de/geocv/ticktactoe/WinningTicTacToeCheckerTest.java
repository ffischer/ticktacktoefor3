package de.geocv.ticktactoe;

import com.google.inject.Guice;
import com.google.inject.Injector;
import de.geocv.ticktactoe.Board.BoardTicTacToe;
import de.geocv.ticktactoe.Board.Figure;
import de.geocv.ticktactoe.Board.SquareBoard;
import de.geocv.ticktactoe.internal.BoardRenderer;
import de.geocv.ticktactoe.internal.Renderer;
import de.geocv.ticktactoe.modules.GamePropertiesModule;
import de.geocv.ticktactoe.modules.GameRunnerModule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class WinningTicTacToeCheckerTest {


    private Injector injector = Guice.createInjector(new GamePropertiesModule(), new GameRunnerModule());
    SquareBoard board;
    WinningChecker winChecker;
    Figure f = new MockFigure();
    Renderer renderer;
    @BeforeEach
    void setUp() {
        board = injector.getInstance(BoardTicTacToe.class);
        winChecker = injector.getInstance(WinningTicTacToeChecker.class);
        renderer = injector.getInstance(BoardRenderer.class);

    }

    @AfterEach
    void tearDown() {
        board = null;
    }

    @Test
    void findHorizontalsAboveLeft() {
        if (isNull(board, winChecker))
            assertTrue(false, "objects are null (board and winchecker)");
        //above left

        board.setFigure(f, 0, 0);
        assertFalse(winChecker.findHorizontals(board, -1, f), "false Win at winChecker");
        board.setFigure(f, 1, 0);
        assertFalse(winChecker.findHorizontals(board, -1, f), "false Win at winChecker");
        board.setFigure(f, 2, 0);
        renderer.draw();
        assertTrue(winChecker.findHorizontals(board, -1, f), "has not detected a winning Game!:(");
    }
    @Test
    void findHorizontalsBottomRight(){
        if (isNull(board, winChecker))
            assertTrue(false, "objects are null (board and winchecker)");

        int x = board.getSize() - 1;

        board.setFigure(f,x,x);
        assertFalse(winChecker.findHorizontals(board,-1,f),"false Win at winChecker");
        board.setFigure(f,x-1,x);
        assertFalse(winChecker.findHorizontals(board,-1,f),"false Win at winChecker");
        board.setFigure(f,x-2,x);
        renderer.draw();
        assertTrue(winChecker.findHorizontals(board,-1,f),"has not detected a winning Game!:(");

    }

    @Test
    void findVerticalsTopLeft() {
        if(isNull(board,winChecker))
            assertTrue(false,"objects are null (board and winchecker)");
        int x = 0;

        board.setFigure(f,x,0);
        assertFalse(winChecker.findHorizontals(board,-1,f),"false Win at winChecker");
        board.setFigure(f,x,1);
        assertFalse(winChecker.findHorizontals(board,-1,f),"false Win at winChecker");
        board.setFigure(f,x,2);
        renderer.draw();
        assertTrue(winChecker.findVerticals(board,-1,f),"has not detected a winning Game!:(");
    }

    @Test
    void findVerticalsBottomRight() {
        if(isNull(board,winChecker))
            assertTrue(false,"objects are null (board and winchecker)");
        int x = board.getSize() - 1;

        board.setFigure(f,x,x);
        assertFalse(winChecker.findHorizontals(board,-1,f),"false Win at winChecker");
        board.setFigure(f,x,x-1);
        assertFalse(winChecker.findHorizontals(board,-1,f),"false Win at winChecker");
        board.setFigure(f,x,x-2);
        renderer.draw();
        assertTrue(winChecker.findVerticals(board,-1,f),"has not detected a winning Game!:(");
    }

    @Test
    void findDiagonalTopLeft() {
        if(isNull(board,winChecker))
            assertTrue(false,"objects are null (board and winchecker)");
        int x = 2;
        int y = 0;

        board.setFigure(f,x,y);
        board.setFigure(f,x-1,y+1);
        board.setFigure(f,x-2,y+2);
        renderer.draw();
        assertTrue(winChecker.findDiagonal(board,-1,f),"has not detected a winning Game!:(");
    }
    @Test
    void findDiagonalTopRight() {
        if(isNull(board,winChecker))
            assertTrue(false,"objects are null (board and winchecker)");
        if(board.getSize() < 4)
            return;

        int x = board.getSize() - 1;
        int y = 0;

        board.setFigure(f,x,y);
        board.setFigure(f,x-1,y+1);
        board.setFigure(f,x-2,y+2);
        renderer.draw();
        assertTrue(winChecker.findDiagonal(board,-1,f),"has not detected a winning Game!:(");
    }
    @Test
    void findDiagonalBottomRight() {
        if(isNull(board,winChecker))
            assertTrue(false,"objects are null (board and winchecker)");
        if(board.getSize() < 4)
            return;

        int x = board.getSize() - 3;
        int y = board.getSize() - 1;

        board.setFigure(f,x,y);
        board.setFigure(f,x+1,y-1);
        board.setFigure(f,x+2,y-2);
        renderer.draw();
        assertTrue(winChecker.findDiagonal(board,-1,f),"has not detected a winning Game!:(");
    }
    @Test
    void findDiagonalBottomLeft() {
        if(isNull(board,winChecker))
            assertTrue(false,"objects are null (board and winchecker)");
        if(board.getSize() < 4)
            return;

        int x = 0;
        int y = board.getSize() - 1;

        board.setFigure(f,x,y);
        board.setFigure(f,x+1,y-1);
        board.setFigure(f,x+2,y-2);
        renderer.draw();
        assertTrue(winChecker.findDiagonal(board,-1,f),"has not detected a winning Game!:(");
    }


    @Test
    void findNegativeDiagonalTopLeft() {
        if(isNull(board,winChecker))
            assertTrue(false,"objects are null (board and winchecker)");
        int x = 0;
        int y = 0;

        board.setFigure(f,x,y);
        board.setFigure(f,x+1,y+1);
        board.setFigure(f,x+2,y+2);
        renderer.draw();
        assertTrue(winChecker.findNegativeDiagonal(board,-1,f),"has not detected a winning Game!:(");

    }


    @Test
    void findNegativeDiagonalBottomRight() {
        if(isNull(board,winChecker))
            assertTrue(false,"objects are null (board and winchecker)");
        int x = board.getSize() - 1;
        int y = board.getSize() - 1;

        board.setFigure(f,x,y);
        board.setFigure(f,x-1,y-1);
        board.setFigure(f,x-2,y-2);
        renderer.draw();
        assertTrue(winChecker.findNegativeDiagonal(board,-1,f),"has not detected a winning Game!:(");

    }

    @Test
    void findNegativeDiagonalBottomLeft() {
        if(isNull(board,winChecker))
            assertTrue(false,"objects are null (board and winchecker)");
        int x = 2;
        int y = board.getSize() - 1;

        board.setFigure(f,x,y);
        board.setFigure(f,x-1,y-1);
        board.setFigure(f,x-2,y-2);
        renderer.draw();
        assertTrue(winChecker.findNegativeDiagonal(board,-1,f),"has not detected a winning Game!:(");

    }
    @Test
    void noWinner(){
        if(isNull(board,winChecker))
            assertTrue(false,"objects are null (board and winchecker)");
        //fill the complete board with figures
        for(int x = 0;  x < board.getSize(); x++){
            for(int y = 0; y < board.getSize(); y++){
                board.setFigure(f,x,y);
            }
        }
        renderer.draw();
        assertTrue(winChecker.noWinner(board),"has not detected a winning Game!:(");

    }

    private boolean isNull(SquareBoard board, WinningChecker winChecker){
        if(board == null || winChecker == null)
            return true;
        return false;

    }
}
