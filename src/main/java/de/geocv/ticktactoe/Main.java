package de.geocv.ticktactoe;

import com.google.inject.Guice;
import com.google.inject.Injector;
import de.geocv.ticktactoe.modules.GamePropertiesModule;
import de.geocv.ticktactoe.modules.GameRunnerModule;

import java.io.FileNotFoundException;

public class Main {


    public static void main(String[] args) throws FileNotFoundException {
        try{
            Injector injector = Guice.createInjector(new GamePropertiesModule(), new GameRunnerModule());
            GameRunnerModule runner = injector.getInstance(GameRunnerModule.class);
            runner.init("./players.json");
        }catch (Exception e){
            System.out.println(e.getStackTrace());
        }
    }
}


