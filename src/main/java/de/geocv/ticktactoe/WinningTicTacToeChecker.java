package de.geocv.ticktactoe;

import de.geocv.ticktactoe.Board.Figure;
import de.geocv.ticktactoe.Board.SquareBoard;
import de.geocv.ticktactoe.enums.WIN_MODES;
import javafx.util.Pair;

public class WinningTicTacToeChecker implements WinningChecker {

    /***
     * +---+---+---+
     * | x | x | x | --> find this pattern
     * +---+---+---+
     * @param board
     * @param amount currently not implemented
     * @param f
     * @return
     */
    @Override
    public boolean findHorizontals(SquareBoard board, int amount, Figure f) {
        for (int x = 0; x < board.getSize(); x++) {
            for (int y = 0; y < board.getSize(); y++) {
                if (board.getFigure(x, y) != null && board.getFigure(x, y) != null && board.getFigure(x, y).getChar() == f.getChar()) {
                    if (board.getFigure(x + 1, y) != null && board.getFigure(x + 1, y).getChar() == f.getChar())
                        if (board.getFigure(x + 2, y) != null && board.getFigure(x + 2, y).getChar() == f.getChar())
                            return true;
                }
            }
        }
        return false;
    }

    /***
     * +---+
     * | X |
     * +---+
     * +---+
     * | X | --> find this vertical pattern
     * +---+
     * +---+
     * | X |
     * +---+
     *  @param board
     * @param amount currently not implemented
     * @param f
     * @return
     */
    @Override
    public boolean findVerticals(SquareBoard board, int amount, Figure f) {
        for (int y = 0; y < board.getSize(); y++) {
            for (int x = 0; x < board.getSize(); x++) {
                if (board.getFigure(x, y) != null && board.getFigure(x, y).getChar() == f.getChar()) {
                    if (board.getFigure(x, y + 1) != null && board.getFigure(x, y + 1).getChar() == f.getChar())
                        if (board.getFigure(x, y + 2) != null && board.getFigure(x, y + 2).getChar() == f.getChar())
                            return true;
                }
            }
        }
        return false;
    }

    /***
     *           +---+
     *           | X |
     *           +---+
     *      +---+
     *      | X |       --->find that pattern
     *      +---+
     * +---+
     * | X |
     * +---+
     * @param board
     * @param amount currently not implemented
     * @param f
     * @return
     */
    @Override
    public boolean findDiagonal(SquareBoard board, int amount, Figure f) {
        for (int y = 0; y < board.getSize(); y++) {
            for (int x = 0; x < board.getSize(); x++) {
                //only interested in two fields from left side and two fields from right side

                if (board.getFigure(x, y) != null && board.getFigure(x, y).getChar() == f.getChar()) {
                    x = x - 1;
                    y = y + 1;
                    Figure bf = board.getFigure(x, y);
                    if (board.getFigure(x, y) != null && board.getFigure(x, y).getChar() == f.getChar())
                        if (board.getFigure(x - 1, y + 1) != null && board.getFigure(x - 1, y + 1).getChar() == f.getChar())
                            return true;

                }

            }
        }

        return false;
    }


    /***
     * +---+
     * | X |
     * +---+
     *      +---+
     *      | X |       --->find that pattern
     *      +---+
     *           +---+
     *           | X |
     *           +---+
     * @param board
     * @param amount currently not implemented
     * @param f
     * @return
     */
    @Override
    public boolean findNegativeDiagonal(SquareBoard board, int amount, Figure f) {
        for (int y = 0; y < board.getSize(); y++) {
            for (int x = 0; x < board.getSize(); x++) {
                if (board.getFigure(x, y) != null && board.getFigure(x, y).getChar() == f.getChar()) {
                    if (board.getFigure(x + 1, y + 1) != null && board.getFigure(x + 1, y + 1).getChar() == f.getChar())
                        if (board.getFigure(x + 2, y + 2) != null && board.getFigure(x + 2, y + 2).getChar() == f.getChar())
                            return true;

                }
            }
        }
        return false;
    }

    /***
     * checks if the board is full so nobody can make a move anymore
     * @param board
     * @return true = board is full - nobody can make a move anymore
     */
    @Override
    public boolean noWinner(SquareBoard board) {
        for(int x = 0; x < board.getSize(); x++){
            for(int y = 0; y < board.getSize(); y++){
                if(board.getFigure(x,y) == null)
                    return false;
            }
        }
        return true;
    }

    /***
     * Wrapper, check for all Win-Modes
     * @param board
     * @param f
     * @return Pair<Boolean, WIN_MODES> if key is false the game continues
     */
    public Pair<Boolean, WIN_MODES> acceptAll(SquareBoard board, Figure f){
        if(findDiagonal(board,-1,f))
            return new Pair<>(true,WIN_MODES.DIAGONAL);
        if(findHorizontals(board,-1,f))
            return new Pair<>(true,WIN_MODES.HORIZONTAL);
        if(findVerticals(board,-1,f))
            return new Pair<>(true,WIN_MODES.VERTICAL);
        if(findNegativeDiagonal(board,-1,f))
            return new Pair<>(true,WIN_MODES.NEGATIVE_DIAGONAL);
        if(noWinner(board))
            return new Pair<>(true,WIN_MODES.NO_WINNER);

        return new Pair<>(false,null);
    }
}
