package de.geocv.ticktactoe.player;

import de.geocv.ticktactoe.Board.Figure;
import de.geocv.ticktactoe.internal.HandleUserValue;

import java.util.Scanner;

public class HumanPlayer extends BasePlayer {


    public HumanPlayer(String name, char sign)
    {
        super(name,sign);
    }

    public void moveStep(Figure figure) {
        boolean isSuccessful = false;
        System.out.println("Player "+this.name+" make your move, please.");
        do {


            System.out.print("X-Value:");
            int x = HandleUserValue.getInteger() - 1;
            if(x > board.getSize()){
                System.out.println("X is bigger than the board-size (it's a square-board");
                continue;
            }

            System.out.print("Y-Value:");
            int y = HandleUserValue.getInteger() - 1;

            if(x > board.getSize() || y > board.getSize()){
                System.out.println("Y is bigger than the board-size - you will be punished a littel bit!");
                continue;
            }

            isSuccessful = board.setFigure(figure,x,y);
            if(!isSuccessful)
                System.out.println("Field is not empty (character:"+board.getFigure(x,y).getChar()+") is here");
        }while (!isSuccessful);

    }


}
