package de.geocv.ticktactoe.player;

import com.google.gson.annotations.Expose;
import de.geocv.ticktactoe.Board.Figure;

import java.util.Random;

public class KIPlayer extends BasePlayer {



    @Expose
    final PlayerFactory.KIMode kiMode;

    public KIPlayer(String name, char sign, PlayerFactory.KIMode kiMode){
        super(name,sign);
        this.kiMode = kiMode;
    }

    public void moveStep(Figure figure) {
        Random rand = new Random();
        if(kiMode == PlayerFactory.KIMode.RANDOM){
            int x = 0;
            int y = 0;
            boolean isSuccessful = false;

            do{
                x = rand.nextInt(board.getSize());
                y = rand.nextInt(board.getSize());
                isSuccessful = board.setFigure(figure,x,y);
            }
            while (!isSuccessful);
            System.out.println(name+" made his move (X:"+x+",Y:"+y+")");
        }
    }


}
