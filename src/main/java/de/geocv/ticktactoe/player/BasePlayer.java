package de.geocv.ticktactoe.player;

import com.google.gson.annotations.Expose;
import com.google.inject.Inject;
import de.geocv.ticktactoe.Board.Figure;
import de.geocv.ticktactoe.Board.SquareBoard;

public abstract class BasePlayer implements Figure {
    @Expose
    protected PlayerFactory.PlayerType playerType;

    public String getName() {
        return name;
    }

    @Expose
    protected final String  name;



    @Expose
    private final char sign;
    @Inject
    protected SquareBoard board;

    /**
     * Don't use it....the PlayersFactory does it for you!!
     * @param b
     */
    public void setBoard(SquareBoard b){
        board = b;
    }
    public abstract void moveStep(Figure figure);

    public BasePlayer(String name, char sign) {
        this.name = name;
        this.sign = sign;
       // this.board = board;
    }

    public char getChar() {
        return sign;
    }
}

