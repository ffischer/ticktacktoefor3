package de.geocv.ticktactoe.player;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import de.geocv.ticktactoe.Board.SquareBoard;
import de.geocv.ticktactoe.internal.RuntimeTypeAdapterFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class PlayerFactory {
    private PlayerFactory() {}
    public enum PlayerType {
        HUMAN,KI;
    }
    public enum KIMode {
        @SerializedName("RANDOM")
        RANDOM;
    }

    private static RuntimeTypeAdapterFactory<BasePlayer> getUserTypeAdapter() {
        return RuntimeTypeAdapterFactory
                .of(BasePlayer.class, "playerType")
                .registerSubtype(HumanPlayer.class, PlayerType.HUMAN.toString())
                .registerSubtype(KIPlayer.class, PlayerType.KI.toString());
    }

    private static RuntimeTypeAdapterFactory<KIPlayer> getKIModeTypeAdapter() {
        return RuntimeTypeAdapterFactory
                .of(KIPlayer.class, "kiMode")
                .registerSubtype(KIPlayer.class, KIMode.RANDOM.toString());
    }

    private static Gson getGsonWithTypeAdapters() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(getUserTypeAdapter());
        builder.registerTypeAdapterFactory(getKIModeTypeAdapter());

        return builder.create();
    }

    public static ArrayList<BasePlayer> create(String path, SquareBoard board) throws FileNotFoundException {
        Type listType = new TypeToken<ArrayList<BasePlayer>>(){}.getType();
        ArrayList<BasePlayer> players = getGsonWithTypeAdapters().fromJson(new FileReader(path), listType);
        for (BasePlayer p: players) {
            p.setBoard(board);
        }
        return players;
    }
}
