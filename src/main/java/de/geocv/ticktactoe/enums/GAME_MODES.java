package de.geocv.ticktactoe.enums;



public enum  GAME_MODES {
    ONE_VS_ONE("OneVsOne"),
    TWO_VS_PC("2VsPC"),
    EGOIST_3("Egoist3");

    private final String text;

    GAME_MODES(String text) { this.text = text; }
    @Override
    public String toString() {
        return text;
    }
}
