package de.geocv.ticktactoe.enums;

public enum START_SEQUENCE {
    HUMAN(0),
    PC(1),
    RANDOM(2);

    private final int id;
    START_SEQUENCE(int id){ this.id = id; }
    public int getValue(){return id;}
}
