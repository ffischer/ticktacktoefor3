package de.geocv.ticktactoe.enums;

public enum WIN_MODES {
    HORIZONTAL("Line from left to right"),
    VERTICAL("Line from top to down"),
    DIAGONAL("oblique line from left down to top right"),
    NEGATIVE_DIAGONAL("oblique line from right down to top left"),
    NO_WINNER("The Gameboard is full and nobody has won");

    private final String text;

    WIN_MODES(String text) { this.text = text; }
    @Override
    public String toString() {
        return text;
    }

}
