package de.geocv.ticktactoe;

import de.geocv.ticktactoe.Board.Figure;
import de.geocv.ticktactoe.Board.SquareBoard;

public interface WinningChecker {
    public boolean findHorizontals(SquareBoard board, int amount, Figure f);
    public boolean findVerticals(SquareBoard board, int amount, Figure f);
    /***
     * from left bottom to right top
     * @param board
     * @return
     */
    public boolean findDiagonal(SquareBoard board, int amount, Figure f);

    /***
     * from right bottom to left top.
     * @param board
     * @return
     */
    public boolean findNegativeDiagonal(SquareBoard board, int amount, Figure f);

    public boolean noWinner(SquareBoard board);

}

