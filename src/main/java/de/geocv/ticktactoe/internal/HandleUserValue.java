package de.geocv.ticktactoe.internal;

import java.util.InputMismatchException;
import java.util.Scanner;

public class HandleUserValue {
    public static int getInteger(){
        Scanner input = new Scanner(System.in);
        boolean continueInput = true;
        int number = 0;
        do {
            try{
                System.out.print("Enter an integer: ");
                number = input.nextInt();

                continueInput = false;
            }
            catch (InputMismatchException ex) {
                System.out.println("Try again. (" +
                        "Incorrect input: an integer is required)");
                input.nextLine();
            }
        }
        while (continueInput);
        return number;
    }


}
