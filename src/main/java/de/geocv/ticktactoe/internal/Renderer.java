package de.geocv.ticktactoe.internal;

public interface Renderer {
    void draw();
}
