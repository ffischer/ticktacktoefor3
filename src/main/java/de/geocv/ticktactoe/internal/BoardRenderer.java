package de.geocv.ticktactoe.internal;

import com.google.inject.Inject;
import de.geocv.ticktactoe.Board.Figure;
import de.geocv.ticktactoe.Board.SquareBoard;

public class BoardRenderer implements Renderer{
    private SquareBoard board;

    @Inject
    public BoardRenderer(SquareBoard b){
        board = b;
    }

    int cellSize = 3;
    private String drawHorizontalBoardLine(){

        char startCharacter ='+';
        char cellFill = '-';
        String line = ""+startCharacter;

        for(int cell = 0; cell < board.getSize(); cell++){
            for(int size = 0; size < cellSize; size++){
                line += cellFill;
            }
            line += startCharacter;
        }

        return line;

    }

    private String fillCell(Figure f){
        String cellVal = "";
        int cellChars = 2;
        int charsEmpty = cellChars / 2;

        //left side
        for(int i = 0; i < charsEmpty; i++){
            cellVal += " ";
        }
        if(f == null)
            cellVal += " ";
        else
            cellVal += f.getChar();
        //right side
        for(int i = 0; i < charsEmpty; i++){
            cellVal += " ";
        }

        return "|"+cellVal;
    }

    public void draw(){

        System.out.println(drawHorizontalBoardLine());

        for(int x = 0; x < board.getSize(); x++){
            for(int y = 0; y < board.getSize(); y++){
                System.out.print(fillCell(board.getFigure(y,x)));
            }
            System.out.print("|");
            System.out.println();
            System.out.println(drawHorizontalBoardLine());

        }
    }
}
