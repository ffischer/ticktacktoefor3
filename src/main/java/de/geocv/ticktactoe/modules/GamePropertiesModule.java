package de.geocv.ticktactoe.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/***
 * ensures that you have access to the /resources/game.properties file in guice through @Named("propertyName")
 */
public class GamePropertiesModule extends AbstractModule {
    /***
     * loads property-file
     * @param name
     * @return
     */
    private static Properties loadProperties(String name){
        Properties properties = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream is = loader.getResourceAsStream(name);
        try {
            properties.load(is);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }finally {
            if(is != null){
                try {
                    is.close();
                } catch (IOException dontCare) { }
            }
        }
        return properties;
    }

    protected void configure() {
        try{
            Properties gameProperties = loadProperties("game.properties");
            Names.bindProperties(binder(),gameProperties);
        }catch (RuntimeException ex){
            addError("Could not configure Game Properties");
        };

    }

}
