package de.geocv.ticktactoe.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import de.geocv.ticktactoe.*;
import de.geocv.ticktactoe.Board.BoardTicTacToe;
import de.geocv.ticktactoe.Board.SquareBoard;
import de.geocv.ticktactoe.enums.GAME_MODES;
import de.geocv.ticktactoe.enums.START_SEQUENCE;
import de.geocv.ticktactoe.enums.WIN_MODES;
import de.geocv.ticktactoe.internal.BoardRenderer;
import de.geocv.ticktactoe.internal.HandleUserValue;
import de.geocv.ticktactoe.internal.Renderer;
import de.geocv.ticktactoe.player.BasePlayer;
import de.geocv.ticktactoe.player.HumanPlayer;
import de.geocv.ticktactoe.player.KIPlayer;
import de.geocv.ticktactoe.player.PlayerFactory;
import javafx.util.Pair;


import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;

/***
 * This class handles the whole game and ties everything together for the tic-tac-toe workflow
 */
public class GameRunnerModule extends AbstractModule {


    @Inject
    SquareBoard board;

    @Inject
    Renderer render;

    @Inject
    WinningChecker winChecker;

    ArrayList<BasePlayer> players;


    START_SEQUENCE starts;
    private GAME_MODES game_mode;

    /***
     * starts the game
     * @param playersConfigFilePath filepath to the Players-Configuration-File. It's in json
     * @throws FileNotFoundException
     */
    public void init(String playersConfigFilePath) throws FileNotFoundException {
        try {
            players = PlayerFactory.create(playersConfigFilePath, board);
        } catch (FileNotFoundException e) {
            throw e;
        }
        isGameValid();
        System.out.println("We play TicTacToe!");
        System.out.println("GameMode:" + game_mode);
        System.out.println("Who starts::" + starts);
        describeGame();
        System.out.println("Let's play!:");
        System.out.println("Coordinate System is 1-based:");
        initOrderOfPlayers();
        render.draw();
        runGame();
    }

    @Override
    protected void configure() {

        binder().bind(Renderer.class).to(BoardRenderer.class);
        binder().bind(SquareBoard.class).to(BoardTicTacToe.class);
        binder().bind(WinningChecker.class).to(WinningTicTacToeChecker.class);
        binder().bind(HandleUserValue.class);
    }

    //because guice can't inject enums :/
    @Inject
    private void getStartSequence(@Named("game.who.starts") String i) {

        starts = START_SEQUENCE.valueOf(i);
    }

    //because guice can't inject enums :/
    @Inject
    private void getGameMode(@Named("game.mode") String i) {

        game_mode = GAME_MODES.valueOf(i);
    }

    private void runGame() {
        boolean hasWon = false;
        do{
            for(BasePlayer p : players){
                p.moveStep(p);
                render.draw();
                //not finished!!
                Pair<Boolean,WIN_MODES> pair = ((WinningTicTacToeChecker)winChecker).acceptAll(board,p);
                if(pair.getKey()){
                    hasWon = true;
                    System.out.println(pair.getValue());
                }
            }
        }while (!hasWon);
    }

    private void describeGame(){
        System.out.println("Here is an Overview of the players");
        for(BasePlayer p : players){
            if(p instanceof HumanPlayer){
                System.out.println("Player's name is "+p.getName() +". Player is human and has character: "+p.getChar());
            }
            if(p instanceof KIPlayer){
                System.out.println("Player's name is "+p.getName() +". Player is a KI, has character: "+p.getChar()+". The KI-Mode is "+ PlayerFactory.KIMode.RANDOM);
            }
        }
    }
    private void initOrderOfPlayers() {
        Pair<Integer, BasePlayer> pair = getHumanPlayer();
        BasePlayer swap;
        switch (starts) {
            case HUMAN:
                if (players.get(0) instanceof HumanPlayer)
                    return;

                swap = players.get(0);
                players.set(0, pair.getValue());
                players.set(pair.getKey(), swap);
                break;

            case PC:
                if (players.get(0) instanceof KIPlayer)
                    return;
                pair = getKIPlayer();
                swap = players.get(0);
                players.set(0, pair.getValue());
                players.set(pair.getKey(), swap);
                break;

            case RANDOM:
                Random rand = new Random();
                int i = 0;
                int a = 0;
                //ensures that a & i have never ever been the equal value.
                while (a == i) {
                    i = rand.nextInt(players.size());
                    a = rand.nextInt(players.size());
                }


                BasePlayer temp = players.get(i);
                swap = players.get(a);
                players.set(a, temp);
                players.set(i, swap);
                break;

            default:
                throw new IllegalArgumentException("Value of WHO_STARTS-Enum is NOT valid - please check in game.properties the prop game.who.starts");


        }

    }

    private Pair<Integer, BasePlayer> getHumanPlayer() {
        Pair<Integer, BasePlayer> pair = null;
        int i = 0;
        for (BasePlayer p : players) {
            if (p instanceof HumanPlayer)
                pair = new Pair<Integer, BasePlayer>(i, p);
            i++;
        }
        if (pair == null)
            throw new IllegalArgumentException("No Human Players in the ArrayList");
        return pair;
    }

    private Pair<Integer, BasePlayer> getKIPlayer() {
        Pair<Integer, BasePlayer> pair = null;
        int i = 0;

        for (BasePlayer p : players) {
            if (p instanceof KIPlayer)
                pair = new Pair<Integer, BasePlayer>(i, p);
            i++;
        }
        if (pair == null)//TODO:put playersConfig-path in properties
            throw new IllegalArgumentException("No PC/KI-Players in the ArrayList, please check ");
        return pair;
    }

    private void isGameValid(){

        if(board.getSize() < 3 || board.getSize() > 10 ){
            throw new IllegalArgumentException("Sorry only board-sizes between 3 - 10 are supported");
        }

        switch (game_mode){
            case EGOIST_3:
                if(players.size() != 3){
                    throw new IllegalArgumentException("Only Games with three Players are allowed");
                }
                break;
            case TWO_VS_PC:
                throw new IllegalArgumentException("Not yet implemented - it is meant that two Players play with the same character. try EGOIST_3");
            case ONE_VS_ONE:
                throw new IllegalArgumentException("Not yet implemented - try EGOIST_3");
        }
    }


}
