package de.geocv.ticktactoe.Board;

public interface SquareBoard{
    public boolean setFigure(Figure f, int x, int y);
    public Figure getFigure(int x, int y);
    public int getSize();

}
