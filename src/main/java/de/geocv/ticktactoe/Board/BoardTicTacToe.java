package de.geocv.ticktactoe.Board;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;


/**
 * Currently board only support quadratic gameBoards
 */
@Singleton
public class BoardTicTacToe implements SquareBoard {

    Figure[][] board;

    @Inject
    public BoardTicTacToe(@Named("game.board.size") int size){
        board = new Figure[size][size];
    }

    /***
     *
     * @param f figure you want to set on your game-board
     * @param x position
     * @param y position
     * @return true = it was successfull, false = field was already set your figure wasn't set
     */
    public boolean setFigure(Figure f, int x, int y){
        //out of range
        if(x >= getSize() || y >= getSize())
            return false;
        //it's only possible to set a board-field if it's empty
        if(isEmpty(x,y)) {
            board[x][y] = f;
            return true;
        }
        else
            return false;

    }

    /***
     * gets a Figure of a Board-Field - very robust. If field is empty returns null
     * @param x negative x-values: returns null; x >= size -> returns null
     * @param y negative y-values: returns null; returns 0; y >= size -> returns null
     * @return
     */
    public Figure getFigure(int x, int y){
        if(x < 0 || y < 0)
            return null;
        if(x < getSize() && y < getSize())
            return board[x][y];
        else
            return null;
    }

    private boolean isEmpty(int x, int y){
        return board[x][y] == null;
    }

    /***
     * x and y have the same size.
     * @return
     */
    public int getSize(){
        return board.length;
    }

}
