#Readme
* Start-Entry: src/main/java/de/geocv/ticktactoe/Main.java
* developed with Java9 (I didn't use any java9-libs on purpose but I've java9 installed and can't gurantee that i used a java9-api by accident)
###General
* IDE: intellij
* use openfx / java 1.9 for pair-type (but I'm unsure if you really need it)
* ~~has a simple but not really tested maven-pom~~ - don't like buildsystems but seems that gradle likes me more than maven.
* Configuration:
  * There are two config-files
  * To build this project as simple as possible these config-files are already in the repository  
    * a game.properties-file (/src/main/resources)
    * a players.json-file (at root - ./) 

###Design-Decisions
* DI (Dependency Injection) gurantees loosly coupled dependencies for easy changing management; maintainable, reusable code (maybe I'm in the mood to write my next Game).
  * Advantage: some more interfaces
  * Disadvantage: a lib more (guice) and more complexity
  * Why not Spring? 
    * I think even DI/Guice is something like throwing cannon-balls on sparrows - Spring, it is definitely!
    * Besides I already have a mini [opensource project](https://bitbucket.org/ffischer/robot-job-application) in springboot
* SOLID-Principles is a purpose here
* KISS-Principles is a purpose here
* Goodold Factory-Pattern just you see i know some patterns ;)
* Why java-property-file?
  * guice converts *.properties-Data almost automatically to @Named("property") - I think that's really nice
  * just i don't forget how to handle java-prop-files
* Why JSON for the players-configuration?
  * json is the de-facto modern internet format for exchanging Data - and maybe i want to build a REST-Service out of it ;)
  * personally I think it's a good compromise between XML(too much to write) and YAML(to less to read it well)
* Why two different configs?
  * Game-Settings in the game.properties are forever / the whole lifecycle of the program think of a webservice
  * Player-Settings could change after every game
  * Currently it's not possible to change the player-settings at runtime or to have more than one game per runtime but it's an small effort for the future ;).    

